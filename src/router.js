import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Dashboard from '@/components/Dashboard'
import NewEmpleado from '@/components/NewEmpleado'
import ViewEmpleado from '@/components/ViewEmpleado'
import EditEmpleado from '@/components/EditEmpleado'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/new',
      name: 'newEmpleado',
      component: NewEmpleado
    },
    {
      path: '/edit/:empleado_id',
      name: 'edit-empleado',
      component: EditEmpleado
    },
    {
      path: '/:empleado_id',
      name: 'view-empleado',
      component: ViewEmpleado
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
